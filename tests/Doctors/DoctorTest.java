package Doctors;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avina on 11/2/2016.
 */
public class DoctorTest {
    @Test
    public void getDoctorName() throws Exception {
        Doctor d = new Doctor("avinash m","gynacologist","7863767045","2","dsnr","345");
        assertEquals("avinash m", d.getDoctorName());
    }

    @Test
    public void getSpeciality() throws Exception {
        Doctor d = new Doctor("avinash m","gynacologist","7863767045","2","dsnr","345");
        assertEquals("gynacologist", d.getSpeciality());
    }

    @Test
    public void getPhoneNumber() throws Exception {
        Doctor d = new Doctor("avinash m","gynacologist","7863767045","2","dsnr","345");
        assertEquals("7863767045", d.getPhoneNumber());
    }

    @Test
    public void getYearOfExperience() throws Exception {
        Doctor d = new Doctor("avinash m","gynacologist","7863767045","2","dsnr","345");
        assertEquals("2", d.getYearOfExperience());
    }

    @Test
    public void getArea() throws Exception {
        Doctor d = new Doctor("avinash m","gynacologist","7863767045","2","dsnr","345");
        assertEquals("dsnr", d.getArea());
    }

    @Test
    public void getReviewScore() throws Exception {
        Doctor d = new Doctor("avinash m","gynacologist","7863767045","2","dsnr","345");
        assertEquals("345", d.getReviewScore());
    }

}