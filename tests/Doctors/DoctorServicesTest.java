package Doctors;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by avina on 11/2/2016.
 */
public class DoctorServicesTest {
    @Test
    public void addDoctor() throws Exception {
    }

    @Test
    public void getSimilarDoctors() throws Exception {
        //get Similar Doctors of search by Name
        DoctorServices docserv = new DoctorServices();
        docserv.addDoctor("avinash m","gynacologist","7863767045","2","san antonio","345");
        docserv.addDoctor("mahesh l","dermatologist","9505685403","3","newyork","234");
        docserv.addDoctor("avinash k","dermatologist","8902838847","3","san francisco","134");
        List<Doctor> docList = docserv.getSimilarDoctors("doctorName","avinash");
        assertEquals("avinash m",docList.get(0).getDoctorName());
        assertEquals("gynacologist",docList.get(0).getSpeciality());
        assertEquals("345",docList.get(0).getReviewScore());
        assertEquals("avinash k",docList.get(1).getDoctorName());
        assertEquals("dermatologist",docList.get(1).getSpeciality());
        assertEquals("134",docList.get(1).getReviewScore());
    }

    @Test
    public void sortByOrder() throws Exception {
        List<Doctor> dataList = new ArrayList<>();
        dataList.add(new Doctor("avinash k","dermatologist","8902838847","3","san francisco","134"));
        dataList.add(new Doctor("avinash m","gynacologist","7863767045","2","san antonio","345"));
        DoctorServices docserv = new DoctorServices();
        List<Doctor> docList = docserv.sortByOrder(dataList);
        assertEquals("avinash m",docList.get(0).getDoctorName());
        assertEquals("gynacologist",docList.get(0).getSpeciality());
        assertEquals("345",docList.get(0).getReviewScore());
        assertEquals("avinash k",docList.get(1).getDoctorName());
        assertEquals("dermatologist",docList.get(1).getSpeciality());
        assertEquals("134",docList.get(1).getReviewScore());
    }

}