package Doctors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by avina on 11/2/2016.
 */
public class DoctorServices {

    static List<Doctor> listOfDoctors = new ArrayList<>();

    //This function add Doctor's data to listOfDoctors
    public void addDoctor(String name, String speciality, String phoneno, String exp, String area, String reviewScore) {
        //Creating the Doctor object
        Doctor doc = new Doctor(name,speciality, phoneno, exp, area, reviewScore);
        listOfDoctors.add(doc);
    }

    //This function return the similar Doctors list(prioritized) based on the choice client or user chooses
    public List<Doctor> getSimilarDoctors(String key, String value) {
        List<Doctor> resultList = new ArrayList<>();
        //based on the choice made by user resultList (NOT prioritized) will be created from listOfDoctors
        for (Doctor doc : listOfDoctors) {
            if (key.equals("doctorName") && doc.getDoctorName().contains(value)) {
                resultList.add(doc);
            } else if (key.equals("speciality")&& doc.getSpeciality().equals(value)) {
                resultList.add(doc);
            } else if (key.equals("area") && doc.getArea().equals(value)) {
                resultList.add(doc);
            } else {
                if (doc.reviewScore.equals(value)) {
                    resultList.add(doc);
                }
            }
        }
        //sending the resultList(NOT prioritized) to sortByOrder()
        return sortByOrder(resultList);
    }

    //This function returns list of Doctors which are prioritized(sorted) by review score
    public List<Doctor> sortByOrder(List<Doctor> list) {
        Collections.sort(list, new Comparator<Doctor>() {
            public int compare(Doctor doc1, Doctor doc2) {
                int score1 = Integer.parseInt(doc1.getReviewScore());
                int score2 = Integer.parseInt(doc2.getReviewScore());
                if (score1 > score2) {
                    return -1;
                } else if(score1 < score2) {
                    return 1;
                } else {
                    //if review scores of both doctors are equal then compare the years of experience of doctors
                    int exp1 = Integer.parseInt(doc1.getYearOfExperience());
                    int exp2 = Integer.parseInt(doc2.getYearOfExperience());
                    if (exp1 > exp2) {
                        return -1;
                    } else if (exp1 < exp2) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        });
        return list;
    }

}
