/**
 * Created by avina on 11/2/2016.
 */
package Doctors;

public class Doctor {
    String doctorName;
    String speciality;
    String phoneNumber;
    String yearOfExperience;
    String area;
    String reviewScore;

    Doctor() {
    }

    //Initializing the Doctor data
    Doctor(String dName, String dspeciality, String dPhoneno, String dExp, String area, String reviewScore) {
        this.doctorName = dName;
        this.speciality = dspeciality;
        this.phoneNumber = dPhoneno;
        this.yearOfExperience = dExp;
        this.area = area;
        this.reviewScore = reviewScore;
    }

    //For retreiving the Doctor data
    public String getDoctorName() {
        return this.doctorName;
    }
    public String getSpeciality() {
        return this.speciality;
    }
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
    public String getYearOfExperience() {
        return this.yearOfExperience;
    }
    public String getArea() {
        return this.area;
    }
    public String getReviewScore() {
        return this.reviewScore;
    }
}
