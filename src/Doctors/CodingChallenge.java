package Doctors;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class CodingChallenge {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        DoctorServices docserv = new DoctorServices();

        //Adding doctors data
        docserv.addDoctor("avinash m","gynacologist","7863767045","2","san antonio","345");
        docserv.addDoctor("mahesh l","dermatologist","9505685403","3","newyork","234");
        docserv.addDoctor("avinash k","gynacologist","8902838847","3","san francisco","134");
        docserv.addDoctor("pavan k","dermatologist","8448984989","4","san francisco","234");

        //User or client chooses choice
        while (true) {
            List<Doctor> similarDoctorsList = new ArrayList<>();
            String str = "";
            System.out.println(" 1. search by name\n 2. search by speciality\n 3. search by area\n 4. Exit");
            System.out.print("Enter the Choice : ");
            String choice = sc.next();
            sc.nextLine();
            if (numberOrNot(choice)) {
                switch(Integer.parseInt(choice)) {
                    case 1 :	System.out.print("enter the name : ");
                        str = sc.nextLine();
                        similarDoctorsList = docserv.getSimilarDoctors("doctorName",str.toLowerCase());
                        break;
                    case 2 :	System.out.print("enter the speciality : ");
                        str = sc.nextLine();
                        similarDoctorsList = docserv.getSimilarDoctors("speciality",str.toLowerCase());
                        break;
                    case 3 :	System.out.print("enter the area : ");
                        str = sc.nextLine();
                        similarDoctorsList = docserv.getSimilarDoctors("area",str.toLowerCase());
                        break;
                    case 4 :	break;
                    default :	System.out.println("Please enter choice (1-4)");
                        break;
                }
                if (Integer.parseInt(choice) == 4) {
                    System.out.println("Thank you for Checking Good Bye!");
                    break;
                }
                if (Integer.parseInt(choice) <= 4 && Integer.parseInt(choice) >= 1) {
                    //Displaying the data filter as per the user choice
                    System.out.println("=================================================================");
                    System.out.println("DoctorName" + "\t" + "ReviewScore" + "\t" + "Speciality" +"\t" + "YearsOfExperience");
                    System.out.println("=================================================================");
                    if (similarDoctorsList.size() == 0) {
                        System.out.println("\t\t\tNo Doctors Found");
                    } else {
                        for (Doctor doc : similarDoctorsList) {
                            System.out.println(doc.getDoctorName() + "\t\t" + doc.getReviewScore() + "\t\t" +
                                    doc.getSpeciality()+"\t\t" + doc.getYearOfExperience());
                        }
                    }
                }
            } else {
                System.out.println("Please Enter Numeric for choice");
            }



            System.out.println();
        }
    }

    //This function validates whether user choice is numeric or not
    public static boolean numberOrNot(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}